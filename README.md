# Hub
Hub is an android application which categorizes notifications based on user-picked constraints.

## Screenshots
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen1.png" width="30%"/>
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen2.png" width="30%"/>
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen3.png" width="30%"/>
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen4.png" width="30%"/>
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen5.png" width="30%"/>
<img src="https://gitlab.com/Eskilop/hub/raw/master/screenshots/hub_screen6.png" width="30%"/>

## License
````
   Copyright 2017-2019 Luca D'Amato

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   ````