/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.support.v4.app.NotificationCompat
import android.util.Log
import it.eskilop.hub.R
import it.eskilop.hub.activities.CategoryActivity
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_CATEGORY
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_EXACT_WORD
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_SENSITIVE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TRIGGER
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_FILTERS
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_NOTIFICATIONS
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Filter
import it.eskilop.hub.models.Notification
import org.jetbrains.anko.defaultSharedPreferences

/**
 * Created by eskilop on 09/06/17.
 */
class NotificationListener : NotificationListenerService() {
    private val TAG = this.javaClass.simpleName
    private var dbm: DbManager = DbManager(this)

    object data : ArrayList<Notification>()

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i("LocalService", "Received start id $startId: $intent")
        return Service.START_STICKY
    }

    override fun onListenerConnected() {
        super.onListenerConnected()
        Log.i(TAG, "connected")
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        val notification = Notification()
        notification.setTitle(nullCheck(sbn.notification.extras.getString(android.app.Notification.EXTRA_TITLE)))
        notification.setMessage(nullCheck(sbn.notification.extras.getString(android.app.Notification.EXTRA_TEXT)))
        notification.setPackage(sbn.packageName)

        if (sbn.notification.contentIntent != null) {
            notification.setIntent(sbn.notification.contentIntent)
        }

        if (nullCheck(sbn.notification.extras.getString(android.app.Notification.EXTRA_BACKGROUND_IMAGE_URI)) != null)
            notification.setMedia(sbn.notification.extras.getString(android.app.Notification.EXTRA_BACKGROUND_IMAGE_URI))
        else {
            if (nullCheck(sbn.notification.extras.getString(android.app.Notification.EXTRA_AUDIO_CONTENTS_URI)) != null) {
                notification.setMedia(sbn.notification.extras.getString(android.app.Notification.EXTRA_AUDIO_CONTENTS_URI))
            }
        }

        if (!checkPackageBlacklisted(notification.getPackage())) {
            for (filter in getFilters()) {
                if (filter.getSensitive()) {
                    if (isSomeHowContained(filter, notification)) {
                        notification.setCategory(filter.getCategory())
                        dbm.save(notification)

                        if (shouldRemoveCategorized()) {
                            cancelNotification(sbn.key)
                        }

                        if (dbm.hasMoreThan(TBL_NAME_NOTIFICATIONS, (Integer.parseInt(defaultSharedPreferences.getString("pref_count_key", "2")) - 1))) {
                            val category = Intent(this@NotificationListener, CategoryActivity::class.java)
                            category.putExtra("category_id", dbm.getId(notification.getCategory()!!))
                            sendNotification(notification, category)
                        }
                    } else // Not contained
                    {
                        if (shouldRemoveUseless()) // Remove useless
                        {
                            cancelNotification(sbn.key)
                        }
                    }
                } else {
                    if (isSomeHowContained(filter, notification)) {
                        notification.setCategory(filter.getCategory())
                        dbm.save(notification)

                        if (shouldRemoveCategorized()) {
                            cancelNotification(sbn.key)
                        }

                        if (dbm.hasMoreThan(TBL_NAME_NOTIFICATIONS, Integer.parseInt(defaultSharedPreferences.getString("pref_count_key", "2")))) {
                            val category = Intent(this@NotificationListener, CategoryActivity::class.java)
                            category.putExtra("category_id", dbm.getId(notification.getCategory()!!))
                            sendNotification(notification, category)
                        }

                    } else // Not contained
                    {
                        if (shouldRemoveUseless()) // Remove useless
                        {
                            cancelNotification(sbn.key)
                        }
                    }
                }
            }
        }

        if (shouldRemoveAll()) {
            cancelNotification(sbn.key)
        }
    }

    private fun isSomeHowContained(filtr: Filter, notification: Notification): Boolean {
        var isc: Boolean
        val trigger = filtr.getTrigger()

        if (filtr.getSensitive()) {
            isc = notification.getTitle().contains(trigger) || notification.getMessage().contains(trigger) && !dbm.contains(notification)

            if (filtr.getExactWord()) {
                isc =
                        (notification.getTitle().contains(" $trigger ") || notification.getMessage().contains(" $trigger ") ||

                                notification.getTitle().contains(" $trigger") || notification.getMessage().contains(" $trigger") ||

                                notification.getTitle().contains("$trigger ") || notification.getMessage().contains("$trigger ")) && !dbm.contains(notification)
            }
        } else {
            var trigger2 = " "


            if (trigger.isNotEmpty()) {
                if (trigger[0].isUpperCase())
                    trigger2 = trigger[0].toLowerCase() + trigger.substring(1, trigger.length)
                else
                    trigger2 = trigger[0].toUpperCase() + trigger.substring(1, trigger.length)
            }

            isc = (notification.getTitle().contains(trigger) || notification.getTitle().contains(trigger2) ||
                    notification.getTitle().contains(trigger.toUpperCase()) || notification.getTitle().contains(trigger.toLowerCase()) ||
                    notification.getMessage().contains(trigger) || notification.getMessage().contains(trigger2) ||
                    notification.getMessage().contains(trigger.toUpperCase()) || notification.getMessage().contains(trigger.toLowerCase())) && !dbm.contains(notification)

            if (filtr.getExactWord()) {
                isc = (notification.getTitle().contains(trigger) || notification.getTitle().contains(trigger2) ||
                        notification.getTitle().contains(trigger.toUpperCase()) || notification.getTitle().contains(trigger.toLowerCase()) ||
                        notification.getMessage().contains(trigger) || notification.getMessage().contains(trigger2) ||
                        notification.getMessage().contains(trigger.toUpperCase()) || notification.getMessage().contains(trigger.toLowerCase()) ||

                        notification.getTitle().contains(" $trigger") || notification.getTitle().contains(" $trigger2") ||
                        notification.getTitle().contains(" $trigger".toUpperCase()) || notification.getTitle().contains(" $trigger".toLowerCase()) ||
                        notification.getMessage().contains(" $trigger") || notification.getMessage().contains(" $trigger2") ||
                        notification.getMessage().contains(" $trigger".toUpperCase()) || notification.getMessage().contains(" $trigger".toLowerCase()) ||

                        notification.getTitle().contains("$trigger ") || notification.getTitle().contains("$trigger2 ") ||
                        notification.getTitle().contains("$trigger ".toUpperCase()) || notification.getTitle().contains("$trigger ".toLowerCase()) ||
                        notification.getMessage().contains("$trigger ") || notification.getMessage().contains("$trigger2 ") ||
                        notification.getMessage().contains("$trigger ".toUpperCase()) || notification.getMessage().contains("$trigger ".toLowerCase())) && !dbm.contains(notification)
            }
        }

        return isc
    }

    private fun shouldRemoveUseless(): Boolean {
        return defaultSharedPreferences.getInt("pref_remove_notifications_key", 3) == 2
    }

    private fun shouldRemoveCategorized(): Boolean {
        return defaultSharedPreferences.getInt("pref_remove_notifications_key", 3) == 1
    }

    private fun shouldRemoveAll(): Boolean {
        return defaultSharedPreferences.getInt("pref_remove_notifications_key", 3) == 0
    }

    private fun getFilters(): ArrayList<Filter> {
        val c = dbm.query(TBL_NAME_FILTERS)
        val filters: ArrayList<Filter> = ArrayList()
        while (c!!.moveToNext()) {
            val s = c.getString(c.getColumnIndex(FIELD_TRIGGER))
            filters.add(Filter(this@NotificationListener, s,
                    dbm.getCategoryFromId(c.getInt(c.getColumnIndex(FIELD_CATEGORY)))!!,
                    c.getString(c.getColumnIndex(FIELD_TITLE)),
                    c.getInt(c.getColumnIndex(FIELD_SENSITIVE)),
                    c.getInt(c.getColumnIndex(FIELD_EXACT_WORD))))
        }
        return filters
    }

    private fun nullCheck(parameter: String?): String? {
        var returnable: String? = null
        if (parameter != null && parameter != "null") {
            returnable = parameter
        }
        return returnable
    }

    private fun checkPackageBlacklisted(pkg: String): Boolean {
        val hardcodedArray = ArrayList<String>()
        hardcodedArray.add("com.android.systemui")
        hardcodedArray.add("it.eskilop.hub")
        hardcodedArray.add("com.android.vending")
        return hardcodedArray.contains(pkg)
    }

    private fun sendNotification(not: Notification, cat: Intent) {
        val alarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val mBuilder = NotificationCompat.Builder(this@NotificationListener)
                .setSmallIcon(R.drawable.ic_hub_notification)
                .setContentTitle(getString(R.string.new_notifications_title))
                .setContentText(getString(R.string.new_notifications_mesg, dbm.countNotificationsFor(dbm.getId(not.getCategory()!!)).toString(), not.getCategory()!!.getTitle()))
                .setContentIntent(PendingIntent.getActivity(this@NotificationListener, 20, cat, PendingIntent.FLAG_CANCEL_CURRENT))
                .setAutoCancel(true)
                .setSound(alarm)
                .setVibrate(longArrayOf(100, 100))

        if (defaultSharedPreferences.getBoolean("pref_notif_max_priority_key", true)) {
            mBuilder.priority = android.app.Notification.PRIORITY_MAX
        }

        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nm.notify(2048, mBuilder.build())
    }
}