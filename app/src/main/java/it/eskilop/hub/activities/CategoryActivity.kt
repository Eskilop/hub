
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.activities

import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.transition.Transition
import android.transition.Transition.TransitionListener
import android.view.View
import android.view.animation.AnimationUtils
import it.eskilop.hub.R
import it.eskilop.hub.adapters.NotificationAdapter
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_CATEGORY
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_MEDIA
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_MESSAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_PACKAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_NOTIFICATIONS
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Notification
import it.eskilop.hub.utils.ImageUtils
import it.eskilop.hub.utils.darken
import it.eskilop.hub.utils.isDark
import it.eskilop.hub.utils.lighten
import kotlinx.android.synthetic.main.activity_category.*
import kotlinx.android.synthetic.main.content_category.*
import org.jetbrains.anko.defaultSharedPreferences


class CategoryActivity : AppCompatActivity() {
    val dbm: DbManager = DbManager(this@CategoryActivity)
    var utils: ImageUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        utils = ImageUtils(this@CategoryActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            for (notification in populate_notification_db()) {
                dbm.delete(notification)
            }
            notification_recycler.adapter = NotificationAdapter(populate_notification_db(), this@CategoryActivity)
        }

        val fade: Transition = window.enterTransition
        val listener: TransitionListener = object : TransitionListener {
            override fun onTransitionResume(p0: Transition?) {
                // fab.visibility = View.INVISIBLE
            }

            override fun onTransitionPause(p0: Transition?) {
            }

            override fun onTransitionCancel(p0: Transition?) {
                // fab.visibility = View.INVISIBLE
            }

            override fun onTransitionStart(p0: Transition?) {
                fab.visibility = View.INVISIBLE
            }

            override fun onTransitionEnd(t: Transition) {
                val anim = AnimationUtils.loadAnimation(this@CategoryActivity, R.anim.abc_fade_in)
                anim.duration = 500
                fab.startAnimation(anim)
                fade.removeListener(this)
                fab.visibility = View.VISIBLE
            }
        }
        fade.addListener(listener)

        val thisCategory = dbm.getCategoryFromId(intent.extras.getInt("category_id"))
        val path = thisCategory!!.getMedia()
        category_picture.setImageDrawable(Drawable.createFromPath(path))
        val palette = utils!!.createPaletteSync(BitmapFactory.decodeFile(path))

        window.statusBarColor = palette.getDominantColor(palette.getLightMutedColor(palette.getDarkMutedColor(resources.getColor(R.color.colorPrimaryDark))))

        // surround with control from settings
        if (defaultSharedPreferences.getBoolean("pref_dynamic_fab_key", true)) {
            fab.backgroundTintList = ColorStateList.valueOf(palette.getVibrantColor(palette.getLightVibrantColor(palette.getDarkVibrantColor(resources.getColor(R.color.colorAccent)))))
        }

        val color: Int
        color = if (isDark(palette.getDominantColor(resources.getColor(R.color.colorPrimaryDark))))
            lighten(palette.getDominantColor(resources.getColor(R.color.colorPrimaryDark)), 0.10)
        else
            darken(palette.getDominantColor(resources.getColor(R.color.colorPrimaryDark)), 0.10)

        toolbar_layout.setContentScrimColor(color)

        if (!isDark(BitmapFactory.decodeFile(path))) {
            toolbar_layout.title = thisCategory.getTitle()
            toolbar_layout.setExpandedTitleColor(Color.argb(200, 0, 0, 0))
            toolbar_layout.setCollapsedTitleTextColor(Color.argb(200, 0, 0, 0))
        } else {
            toolbar_layout.title = thisCategory.getTitle()
            toolbar_layout.setExpandedTitleColor(Color.WHITE)
            toolbar_layout.setCollapsedTitleTextColor(Color.WHITE)
        }

        val notification_recycler = findViewById(R.id.notification_recycler) as RecyclerView
        notification_recycler.setHasFixedSize(true)
        notification_recycler.layoutManager = LinearLayoutManager(applicationContext)
        notification_recycler.isNestedScrollingEnabled = false
        notification_recycler.adapter = NotificationAdapter(populate_notification_db(), this@CategoryActivity)
        val mIth = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: ViewHolder, target: ViewHolder): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                dbm.delete((notification_recycler.adapter as NotificationAdapter).getElementAt(viewHolder.position))
                notification_recycler.adapter = NotificationAdapter(populate_notification_db(), this@CategoryActivity)
            }
        })

        mIth.attachToRecyclerView(notification_recycler)

    }

    fun populate_notification_db(): ArrayList<Notification> {
        val c = dbm.query(TBL_NAME_NOTIFICATIONS)!!
        val data: ArrayList<Notification> = ArrayList()

        while (c.moveToNext()) {
            if (c.getInt(c.getColumnIndex(FIELD_CATEGORY)) == intent.extras.get("category_id")) {

                val n = Notification(c.getString(c.getColumnIndex(FIELD_TITLE)),
                        c.getString(c.getColumnIndex(FIELD_MESSAGE)),
                        c.getString(c.getColumnIndex(FIELD_MEDIA)),
                        c.getString(c.getColumnIndex(FIELD_PACKAGE))
                )
                data.add(n)
            }
        }
        return data
    }

    override fun onBackPressed() {
        super.onBackPressed()
        fab.visibility = View.INVISIBLE
        finishAfterTransition()
    }
}
