
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.activities

import android.app.AlertDialog
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceManager
import android.preference.PreferenceScreen
import android.preference.SwitchPreference
import it.eskilop.hub.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.editText
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast


class SettingsActivity : it.eskilop.hub.activities.AppCompatPreferenceActivity() {
    var prefs: SharedPreferences? = null

    val PREF_COUNT_KEY = "pref_count_key"
    val PREF_REMOVE_NOTIFICATIONS_KEY = "pref_remove_notifications_key"
    val PREF_THEME_KEY = "pref_theme_key"
    val PREF_DYNAMIC_FAB_KEY = "pref_dynamic_fab_key"
    val PREF_NOTIF_MAX_PRIORITY_KEY = "pref_notif_max_priority_key"

    var elements: List<String> = listOf("")

    var notiprefs: Array<String> = Array<String>(0, { i -> i.toString() })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.pref_general)

        elements = listOf(resources.getString(R.string.theme_clear),
                resources.getString(R.string.theme_dark),
                resources.getString(R.string.theme_black))

        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        notiprefs = arrayOf(getString(R.string.notipref_remove_all), getString(R.string.notipref_remove_saved), getString(R.string.notipref_remove_useless), getString(R.string.notipref_remove_none))

        findPreference(PREF_COUNT_KEY).summary = prefs!!.getString(PREF_COUNT_KEY, "2")
        findPreference(PREF_COUNT_KEY).onPreferenceClickListener = listener
        (findPreference(PREF_REMOVE_NOTIFICATIONS_KEY) as PreferenceScreen).summary = notiprefs[prefs!!.getInt(PREF_REMOVE_NOTIFICATIONS_KEY, 3)]
        findPreference(PREF_REMOVE_NOTIFICATIONS_KEY).onPreferenceClickListener = listener
        (findPreference(PREF_NOTIF_MAX_PRIORITY_KEY) as SwitchPreference).isChecked = prefs!!.getBoolean(PREF_NOTIF_MAX_PRIORITY_KEY, true)
        findPreference(PREF_NOTIF_MAX_PRIORITY_KEY).onPreferenceClickListener = listener
        //findPreference(PREF_THEME_KEY).summary = elements[Integer.parseInt(prefs!!.getString(PREF_THEME_KEY, "0"))]
        //findPreference(PREF_THEME_KEY).onPreferenceClickListener = listener
        (findPreference(PREF_DYNAMIC_FAB_KEY) as SwitchPreference).isChecked = prefs!!.getBoolean(PREF_DYNAMIC_FAB_KEY, true)
        findPreference(PREF_DYNAMIC_FAB_KEY).onPreferenceClickListener = listener
        findPreference("see_tutorial").onPreferenceClickListener = listener

    }

    var listener: Preference.OnPreferenceClickListener = Preference.OnPreferenceClickListener { preference ->
        when (preference.key) {
            PREF_COUNT_KEY -> {
                alert {
                    this.title(getString(R.string.pref_count_notifier))
                    customView {
                        val et = editText {
                            hint = prefs!!.getString(PREF_COUNT_KEY, "Default : 2")
                        }
                        et.setRawInputType(Configuration.KEYBOARD_QWERTY)
                        positiveButton {
                            updatePreference(PREF_COUNT_KEY, et.text.toString())
                            preference.summary = prefs!!.getString(PREF_COUNT_KEY, "2")
                        }
                    }
                }.show()
                true
            }

            "see_tutorial" -> {
                updatePreference("isFirstTime", true)
                toast(resources.getString(R.string.tut1_share))
                true
            }

            PREF_REMOVE_NOTIFICATIONS_KEY -> {
                AlertDialog.Builder(this@SettingsActivity)
                        .setTitle(R.string.notipref_title)
                        .setItems(notiprefs,
                                { dialogInterface, which ->
                                    updatePreference(PREF_REMOVE_NOTIFICATIONS_KEY, which)
                                    reloadPreference(preference, null, notiprefs[which])
                                }).show()
                true
            }

            PREF_NOTIF_MAX_PRIORITY_KEY -> {
                updatePreference(PREF_NOTIF_MAX_PRIORITY_KEY, (preference as SwitchPreference).isChecked)
                true
            }

            PREF_THEME_KEY -> {
                selector("", elements)
                { i ->
                    updatePreference(PREF_THEME_KEY, i.toString())
                    preference.summary = elements[i]
                }
                true
            }
            PREF_DYNAMIC_FAB_KEY -> {
                updatePreference(PREF_DYNAMIC_FAB_KEY, (preference as SwitchPreference).isChecked)
                true
            }
            else -> {
                true
            }
        }
    }

    fun updatePreference(id: String, value: Any) {
        when (value) {
            is Boolean -> {
                prefs!!.edit()
                        .putBoolean(id, value)
                        .apply()
            }

            is Int -> {
                prefs!!.edit()
                        .putInt(id, value)
                        .apply()
            }

            is String -> {
                prefs!!.edit()
                        .putString(id, value)
                        .apply()
            }
        }
    }

    fun reloadPreference(preference: Preference, value: Any?, summary: String) {
        when (preference) {
            is SwitchPreference -> {
                preference.isChecked = value as Boolean
                preference.summary = summary
            }

            is PreferenceScreen -> {
                preference.summary = summary
            }
        }
    }
}
