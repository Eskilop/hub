
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.activities

import android.database.Cursor
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import it.eskilop.hub.R
import it.eskilop.hub.adapters.FiltersAdapter
import it.eskilop.hub.data.Database
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_CATEGORY
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Filter
import kotlinx.android.synthetic.main.activity_filters.*
import org.jetbrains.anko.defaultSharedPreferences

class FiltersActivity : AppCompatActivity() {
    var category: Int = 0
    var dbm: DbManager = DbManager(this@FiltersActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)

        category = intent.extras.get("category_id") as Int

        recycler_filters.setHasFixedSize(true)
        recycler_filters.layoutManager = LinearLayoutManager(this@FiltersActivity)
        recycler_filters.adapter = FiltersAdapter(getData(), this@FiltersActivity)

        val mIth = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                dbm.delete((recycler_filters.adapter as FiltersAdapter).getElementAt(viewHolder.position))
                recycler_filters.adapter = FiltersAdapter(getData(), this@FiltersActivity)
            }
        })

        mIth.attachToRecyclerView(recycler_filters)

        if (defaultSharedPreferences.getBoolean("isFirstTimeFilterHint", true))
            run {
                Snackbar.make(
                        filter_view,
                        resources.getString(R.string.tut_filters_view),
                        Snackbar.LENGTH_LONG
                ).show()
                defaultSharedPreferences.edit().putBoolean("isFirstTimeFilterHint", false).apply()
            }
    }

    fun getData(): ArrayList<Filter> {
        val dbm: DbManager = DbManager(this)
        val c: Cursor? = dbm.query(Database.DatabaseStrings.TBL_NAME_FILTERS)
        val elements = ArrayList<Filter>()
        while (c!!.moveToNext()) {
            if (c.getInt(c.getColumnIndex(FIELD_CATEGORY)) == category) {
                elements.add(Filter(applicationContext, c.getString(c.getColumnIndex(Database.DatabaseStrings.FIELD_TRIGGER)), dbm.getCategoryFromId(c.getInt(c.getColumnIndex(Database.DatabaseStrings.FIELD_CATEGORY)))!!, c.getString(c.getColumnIndex(Database.DatabaseStrings.FIELD_TITLE)), c.getInt(c.getColumnIndex(Database.DatabaseStrings.FIELD_SENSITIVE)), c.getInt(c.getColumnIndex(Database.DatabaseStrings.FIELD_EXACT_WORD))))
            }
        }
        return elements
    }
}
