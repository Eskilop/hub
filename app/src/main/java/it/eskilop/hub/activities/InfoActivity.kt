
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.activities

import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import it.eskilop.hub.R
import it.eskilop.hub.adapters.LicensesAdapter
import it.eskilop.hub.models.License
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val data: ArrayList<License> = ArrayList()

        // fab-speed-dial
        data.add(License("Yavor Ivanov", "fab-speed-dial", "https://github.com/yavski/fab-speed-dial",
                "Copyright 2016 Yavor Ivanov\n\n" +
                        "Licensed under the Apache License, Version 2.0 (the \"License\");\n " +
                        "you may not use this file except in compliance with the License.\n " +
                        "You may obtain a copy of the License at\n\n " +
                        "" +
                        "http://www.apache.org/licenses/LICENSE-2.0\n\n " +
                        "" +
                        "Unless required by applicable law or agreed to in writing, software\n " +
                        "distributed under the License is distributed on an \"AS IS\" BASIS,\n " +
                        "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n " +
                        "See the License for the specific language governing permissions and\n " +
                        "limitations under the License."))

        // FancyShowcaseView
        data.add(License("Faruk Toptaş", "FancyShowCaseView", "https://github.com/faruktoptas/FancyShowCaseView",
                "Copyright 2017 Faruk Toptaş\n\n" +
                        "Licensed under the Apache License, Version 2.0 (the \"License\");\n " +
                        "you may not use this file except in compliance with the License.\n " +
                        "You may obtain a copy of the License at\n\n " +
                        "" +
                        "http://www.apache.org/licenses/LICENSE-2.0\n\n " +
                        "" +
                        "Unless required by applicable law or agreed to in writing, software\n " +
                        "distributed under the License is distributed on an \"AS IS\" BASIS,\n " +
                        "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n " +
                        "See the License for the specific language governing permissions and\n " +
                        "limitations under the License."))

        licenses_recycler.layoutManager = LinearLayoutManager(this)
        licenses_recycler.setHasFixedSize(true)
        licenses_recycler.adapter = LicensesAdapter(data)

        app_icon_btn.setOnClickListener {
            CustomTabsIntent.Builder()
                    .setToolbarColor(resources.getColor(R.color.colorPrimary))
                    .build()
                    .launchUrl(this@InfoActivity, Uri.parse("http://www.eskilop.it"))
        }
    }
}
