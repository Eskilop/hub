
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.data

/**
 * Created by eskilop on 30/05/17.
 */
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class Database(context: Context) : SQLiteOpenHelper(context, Database.DB_NAME, null, 1) {
    object DatabaseStrings {
        const val FIELD_ID = "_id"
        const val FIELD_CATEGORY = "category"
        const val TBL_NAME_NOTIFICATIONS = "Notifs"
        const val FIELD_TITLE = "title"
        const val FIELD_MESSAGE = "message"
        const val FIELD_MEDIA = "media"
        const val FIELD_PACKAGE = "package"
        const val FIELD_TIMESTAMP = "timestamp"
        const val FIELD_INTENT = "intent"
        const val TBL_NAME_FILTERS = "Filters"
        const val FIELD_TRIGGER = "trigger"
        const val FIELD_SENSITIVE = "caseSensitive"
        const val FIELD_EXACT_WORD = "exactWord"
        const val TBL_NAME_CATEGORIES = "Categories"
        const val FIELD_IMAGE = "image"
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Create notifications table
        val notifs = "CREATE TABLE " + DatabaseStrings.TBL_NAME_NOTIFICATIONS +
                " ( " + DatabaseStrings.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DatabaseStrings.FIELD_TITLE + " TEXT, " +
                DatabaseStrings.FIELD_MESSAGE + " TEXT, " +
                DatabaseStrings.FIELD_MEDIA + " TEXT, " +
                DatabaseStrings.FIELD_PACKAGE + " TEXT, " +
                DatabaseStrings.FIELD_TIMESTAMP + " TEXT, " +
                DatabaseStrings.FIELD_INTENT + " BLOB, " +
                DatabaseStrings.FIELD_CATEGORY + " INTEGER, " +
                "FOREIGN KEY (${DatabaseStrings.FIELD_CATEGORY}) REFERENCES ${DatabaseStrings.TBL_NAME_CATEGORIES}(${DatabaseStrings.FIELD_ID}))"
        db.execSQL(notifs)

        // Create filters table
        val filters = "CREATE TABLE " + DatabaseStrings.TBL_NAME_FILTERS +
                " ( " + DatabaseStrings.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseStrings.FIELD_TITLE + " TEXT, " +
                DatabaseStrings.FIELD_TRIGGER + " TEXT, " +
                DatabaseStrings.FIELD_SENSITIVE + " INTEGER, " +
                DatabaseStrings.FIELD_EXACT_WORD + " INTEGER, " + // exact word: .contains(" word ")
                DatabaseStrings.FIELD_CATEGORY + " INTEGER, " +
                "FOREIGN KEY (${DatabaseStrings.FIELD_CATEGORY}) REFERENCES ${DatabaseStrings.TBL_NAME_CATEGORIES}(${DatabaseStrings.FIELD_ID}))"
        db.execSQL(filters)
        // create categories table
        val categories = "CREATE TABLE " + DatabaseStrings.TBL_NAME_CATEGORIES +
                " ( " + DatabaseStrings.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DatabaseStrings.FIELD_TITLE + " TEXT," +
                DatabaseStrings.FIELD_IMAGE + " TEXT)"
        db.execSQL(categories)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    companion object {
        const val DB_NAME = "Store"
    }
}