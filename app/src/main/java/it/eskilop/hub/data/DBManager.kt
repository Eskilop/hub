
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.data

/**
 * Created by eskilop on 30/05/17.
 */
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.os.Parcel
import android.util.Log
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_CATEGORY
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_EXACT_WORD
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_ID
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_IMAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_INTENT
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_MEDIA
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_MESSAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_PACKAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_SENSITIVE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TIMESTAMP
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TRIGGER
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_CATEGORIES
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_FILTERS
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_NOTIFICATIONS
import it.eskilop.hub.models.Category
import it.eskilop.hub.models.Filter
import it.eskilop.hub.models.Notification
import java.io.File

class DbManager(ctx: Context) {
    val context = ctx
    private val database: Database = Database(ctx)

    fun save(obj: Any) {
        Log.i("DBM", "Saving")
        val db = database.writableDatabase
        val cv = ContentValues()
        var table = "?"

        when (obj) {
            is Filter -> {
                cv.put(FIELD_TITLE, obj.getTitle())
                cv.put(FIELD_TRIGGER, obj.getTrigger())
                cv.put(FIELD_SENSITIVE, obj.getSensitive())
                cv.put(FIELD_EXACT_WORD, obj.getExactWord())
                cv.put(FIELD_CATEGORY, getId(obj.getCategory()))
                table = TBL_NAME_FILTERS
            }
            is Notification -> {
                cv.put(FIELD_TITLE, obj.getTitle())
                cv.put(FIELD_MESSAGE, obj.getMessage())
                cv.put(FIELD_MEDIA, obj.getMedia())
                cv.put(FIELD_PACKAGE, obj.getPackage())
                cv.put(FIELD_CATEGORY, getId(obj.getCategory()!!))

                table = TBL_NAME_NOTIFICATIONS
            }
            is Category -> {
                cv.put(FIELD_TITLE, obj.getTitle())
                cv.put(FIELD_IMAGE, obj.getMedia())
                table = TBL_NAME_CATEGORIES
            }
        }

        try {
            if (!contains(obj)) {
                db.insert(table, null, cv)
            }
        } catch (sqle: SQLiteException) {
            Log.i("DBManager: ", "error during category insertion")
        }
    }

    fun update(table: String, cv: ContentValues, obj: Any) {
        val db = database.writableDatabase
        db.update(table, cv, FIELD_ID + "=" + getId(obj), null)
    }

    fun isEmpty(table: String): Boolean {
        val crs: Cursor? = query(table)
        return crs!!.count == 0
    }

    fun hasOne(table: String): Boolean {
        val crs: Cursor? = query(table)
        return crs!!.count == 1
    }

    val hasMoreThanOne = {table:String -> hasMoreThan(table, 1)}

    fun hasMoreThan(table: String, n: Int): Boolean {
        val crs: Cursor? = query(table)
        return crs!!.count > n
    }

    fun countNotificationsFor(category: Int): Int {
        val db = database.readableDatabase
        val crs: Cursor? = db.rawQuery("SELECT * FROM $TBL_NAME_NOTIFICATIONS WHERE $FIELD_CATEGORY = '$category'", null)
        return crs!!.count
    }

    fun query(table: String): Cursor? {
        var crs: Cursor? = null
        try {
            val db = database.readableDatabase
            crs = db.query(table, null, null, null, null, null, null)
        } catch (sqle: SQLiteException) {
            Log.i("DBManager", "error during query")
            return null
        }
        return crs
    }

    fun contains(obj: Any): Boolean {
        val db = database.readableDatabase
        var c: Cursor? = query(TBL_NAME_CATEGORIES)

        try {
            when (obj) {
                is Notification -> {
                    c = db.rawQuery("SELECT _id FROM $TBL_NAME_NOTIFICATIONS WHERE $FIELD_TITLE = '" + obj.getTitle().replace("'", "''") + "' AND $FIELD_MESSAGE = '" + obj.getMessage().replace("'", "''") + "'", null)
                }
                is Category -> {
                    c = db.rawQuery("SELECT _id FROM $TBL_NAME_CATEGORIES  WHERE $FIELD_TITLE = '" + obj.getTitle() + "' AND $FIELD_IMAGE  = '" + obj.getMedia() + "'", null)
                }
                is Filter -> {
                    c = db.rawQuery("SELECT _id FROM $TBL_NAME_FILTERS  WHERE $FIELD_TITLE = '" + obj.getTitle() + "' AND $FIELD_TRIGGER = '" + obj.getTrigger() + "'", null)
                }
            }
        } catch (sqle: SQLiteException) {
            Log.i("Error during contains()", sqle.message)
        }
        return c!!.count > 0
    }

    fun nestedDelete(category: Category?) {
        var crs = query(TBL_NAME_NOTIFICATIONS)
        while (crs!!.moveToNext()) {

            if (crs.getString(crs.getColumnIndex(FIELD_CATEGORY)) == category!!.getTitle()) {
                delete(Notification(crs.getString(crs.getColumnIndex(FIELD_TITLE)),
                        crs.getString(crs.getColumnIndex(FIELD_MESSAGE)),
                        crs.getString(crs.getColumnIndex(FIELD_MEDIA)),
                        crs.getString(crs.getColumnIndex(FIELD_PACKAGE))))
            }
        }
        crs = query(TBL_NAME_FILTERS)
        while (crs!!.moveToNext()) {
            if (crs.getString(crs.getColumnIndex(FIELD_CATEGORY)) == category!!.getTitle()) {
                val trigger = crs.getString(crs.getColumnIndex(FIELD_TRIGGER))
                val cate = getCategoryFromId(crs.getInt(crs.getColumnIndex(FIELD_CATEGORY)))
                val name = crs.getString(crs.getColumnIndex(FIELD_TITLE))
                val sensitive = crs.getInt(crs.getColumnIndex(FIELD_SENSITIVE))
                val exact = crs.getInt(crs.getColumnIndex(FIELD_EXACT_WORD))

                delete(Filter(context, trigger, cate!!, name, sensitive, exact))
            }
        }
        delete(category!!)
        val f = File(category.getMedia())
        f.deleteRecursively()
    }

    fun delete(obj: Any): Boolean {
        val db = database.writableDatabase
        var success: Boolean = false
        try {
            if (getId(obj) != 999999) {
                when (obj) {
                    is Notification -> {
                        success = db.delete(TBL_NAME_NOTIFICATIONS, "$FIELD_ID=${getId(obj)}", null) < 0
                    }
                    is Category -> {
                        success = db.delete(TBL_NAME_CATEGORIES, "$FIELD_ID=${getId(obj)}", null) < 0
                    }
                    is Filter -> {
                        success = db.delete(TBL_NAME_FILTERS, "$FIELD_ID=${getId(obj)}", null) < 0
                    }
                }
            }
        } catch (sqle: SQLiteException) {
            Log.i("DBManager", "error during deletion: " + sqle.message)
            return false
        }
        return success
    }

    fun getCategoryFromId(id: Int): Category? {
        val db = database.readableDatabase
        var title = ""
        var image = ""
        val crs = db.rawQuery("SELECT * FROM $TBL_NAME_CATEGORIES WHERE $FIELD_ID = $id", null)!!
        if (crs.count > 0) {
            crs.moveToFirst()
            title = crs.getString(crs.getColumnIndex(FIELD_TITLE))
            image = crs.getString(crs.getColumnIndex(FIELD_IMAGE))
        }
        return Category(title, image)
    }

    fun getId(obj: Any): Int {
        val db = database.readableDatabase
        var c: Cursor? = null
        var id = 999999

        when (obj) {
            is Notification -> {
                c = db.rawQuery("SELECT _id FROM $TBL_NAME_NOTIFICATIONS WHERE $FIELD_TITLE = '" + obj.getTitle().replace("'", "''") + "' AND $FIELD_MESSAGE = '" + obj.getMessage().replace("'", "''") + "'", null)
            }
            is Category -> {
                c = db.rawQuery("SELECT _id FROM $TBL_NAME_CATEGORIES  WHERE $FIELD_TITLE = '" + obj.getTitle() + "' AND $FIELD_IMAGE  = '" + obj.getMedia() + "'", null)
            }
            is Filter -> {
                c = db.rawQuery("SELECT _id FROM $TBL_NAME_FILTERS  WHERE $FIELD_TITLE = '" + obj.getTitle() + "' AND $FIELD_TRIGGER = '" + obj.getTrigger() + "'", null)
            }
        }

        if (c!!.moveToFirst()) {
            id = c.getInt(0)
        } else {
            Log.e("Error while getting id", c.count.toString())
        }
        c.close()
        return id
    }
}