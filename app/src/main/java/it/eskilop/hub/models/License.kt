
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.models

/**
 * Created by eskilop on 6/27/17.
 */
class License(devName: String, libName: String, sourceCodeUrl: String, licenseText: String) {
    val dev_name = devName
    val source_code_url = sourceCodeUrl
    val license_text = licenseText
    val lib_name = libName

    fun getDevName(): String {
        return dev_name
    }

    fun getLibName(): String {
        return lib_name
    }

    fun getSourceCodeUrl(): String {
        return source_code_url
    }

    fun getLicenseText(): String {
        return license_text
    }
}