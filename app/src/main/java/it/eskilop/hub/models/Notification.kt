
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.models

import android.app.PendingIntent

/**
 * Created by eskilop on 08/06/17.
 */
class Notification(title: String = "None", message: String = "None", media: String = "", pkg: String = "", intent: PendingIntent? = null, category: Category? = null) {
    private var mTitle: String = title
    private var mMessage: String = message
    private var mMedia: String = media
    private var mPackage = pkg
    private var mCategory = category
    private var mIntent: PendingIntent? = intent
    lateinit private var mTimestamp: String

    fun getTitle(): String {
        return mTitle
    }

    fun getMessage(): String {
        return mMessage
    }

    fun getCategory(): Category? {
        return mCategory
    }

    fun setTitle(title: String?) {
        if (title != null) {
            mTitle = title
        }
    }

    fun setMessage(message: String?) {
        if (message != null) {
            mMessage = message
        }
    }

    fun setCategory(category: Category) {
        mCategory = category
    }

    fun setMedia(mediaPath: String) {
        mMedia = mediaPath
    }

    fun getMedia(): String {
        return mMedia
    }

    fun setPackage(pkg: String) {
        mPackage = pkg
    }

    fun getPackage(): String {
        return mPackage
    }

    fun setIntent(intent: PendingIntent) {
        mIntent = intent
    }

    fun getIntent(): PendingIntent? {
        return mIntent
    }

    override fun toString(): String {
        return "Notification(mTitle='$mTitle', mMessage='$mMessage', mCategory='$mCategory')"
    }

    fun setTimestamp(timestamp: String) {
        mTimestamp = timestamp
    }

    fun getTimestamp(): String {
        return mTimestamp
    }
}