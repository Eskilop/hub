
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.models

import android.content.Context

/**
 * Created by eskilop on 12/06/17.
 */
class Filter(context: Context, trigger: String, category: Category, title: String = "Generic Filter", sensitive: Int, exact: Int) {
    private var mTitle: String = title
    private var mTrigger: String = trigger
    private var mCategory: Category = category
    private var mSensitive: Int = sensitive
    private var mExactWord: Int = exact

    fun getTitle(): String {
        return mTitle
    }

    fun getTrigger(): String {
        return mTrigger
    }

    fun getCategory(): Category {
        return mCategory
    }

    fun getIntegerSensitive(): Int {
        return mSensitive
    }

    fun getIntegerExact(): Int {
        return mExactWord
    }

    fun getSensitive(): Boolean {
        return mSensitive != 0
    }

    fun getExactWord(): Boolean {
        return mExactWord != 0
    }

    override fun toString(): String {
        return "Filter(mName='$mTitle', mTrigger='$mTrigger', mCategory='$mCategory')"
    }
}