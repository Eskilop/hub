
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.adapters

import android.app.ActivityOptions
import android.content.ContentValues
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.app.AlertDialog
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import it.eskilop.hub.HubActivity
import it.eskilop.hub.R
import it.eskilop.hub.activities.CategoryActivity
import it.eskilop.hub.activities.FiltersActivity
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_IMAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_CATEGORIES
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Category
import it.eskilop.hub.utils.ImageUtils
import kotlinx.android.synthetic.main.activity_hub.*
import kotlinx.android.synthetic.main.category.view.*
import kotlinx.android.synthetic.main.dialog_category.view.*
import org.jetbrains.anko.startActivity


/**
 * Created by eskilop on 07/06/17.
 */
class CategoryAdapter(data: ArrayList<Category>, activity: HubActivity) : RecyclerView.Adapter<CategoryAdapter.CategoryHolder>() {
    var mData: ArrayList<Category> = data
    val mActivity: HubActivity = activity
    var dbm: DbManager = DbManager(activity)
    var lastPosition = -1


    class CategoryHolder(v: View, activity: HubActivity) : RecyclerView.ViewHolder(v), View.OnClickListener {
        val mTitle = v.category_title as TextView
        val mMedia = v.category_image as ImageView
        val mMenuBtn = v.category_menu as ImageButton
        val mPopupMenu = PopupMenu(v.context, mMenuBtn)
        val mRootLayout = v
        val menuInflater = mPopupMenu.menuInflater
        val menu: MenuBuilder = mPopupMenu.menu as MenuBuilder
        var category: Category? = null
        val mActivity: HubActivity = activity
        var dbm: DbManager = DbManager(activity)

        init {
            menu.setOptionalIconsVisible(true)

            menuInflater.inflate(R.menu.category_menu, mPopupMenu.menu)
            mMenuBtn.setOnClickListener { mPopupMenu.show() }

            v.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val i = Intent(v!!.context, CategoryActivity::class.java)
            i.putExtra("category_id", dbm.getId(category!!))

            val options = ActivityOptions.makeSceneTransitionAnimation(mActivity, mMedia as View, "sharedElemsTransition")

            v.context.startActivity(i, options.toBundle())
        }

        fun clearAnimation() {
            mRootLayout.clearAnimation()
        }
    }

    override fun onBindViewHolder(p0: CategoryHolder, p1: Int) {
        p0.category = mData.get(p1)
        p0.mTitle.text = mData.get(p1).getTitle()
        mActivity.pickedImage = BitmapFactory.decodeFile(mData.get(p1).getMedia())
        p0.mMedia.setImageBitmap(mActivity.pickedImage)

        mActivity.pickedImage = BitmapFactory.decodeResource(mActivity.resources, R.drawable.defaultimage)

        setAnimation(p0.itemView, p1)

        p0.mPopupMenu.setOnMenuItemClickListener { item ->
            when (item!!.itemId) {
                R.id.menu_remove -> {

                    val removing = Category(mData.get(p1).getTitle(), mData.get(p1).getMedia())

                    AlertDialog.Builder(mActivity)
                            .setTitle(mActivity.getString(R.string.alert_delete_title))
                            .setMessage(mActivity.getString(R.string.alert_delete_message, removing.getTitle()))
                            .setPositiveButton(mActivity.getString(R.string.y)) { _, _ ->
                                if (dbm.hasOne(TBL_NAME_CATEGORIES)) {
                                    mActivity.layoutInsert()
                                }

                                dbm.nestedDelete(removing)
                                mActivity.recycler.adapter = CategoryAdapter(mActivity.populateCategoriesDb(), mActivity)
                            }
                            .setNegativeButton(mActivity.getString(R.string.n)) { _, _ ->

                            }
                            .create().show()
                }
                R.id.menu_edit -> {
                    var imageChanged = false
                    val v: View = mActivity.layoutInflater.inflate(R.layout.dialog_category, null)
                    val t: EditText = (v.category_et as EditText)

                    t.setText(mData.get(p1).getTitle())

                    (v.category_imageb as ImageButton).setOnClickListener {
                        mActivity.getImage()
                        imageChanged = true
                        (v.category_imageb as ImageButton).setImageDrawable(mActivity.resources.getDrawable(R.drawable.tick))
                    }

                    AlertDialog.Builder(mActivity)
                            .setTitle(mActivity.resources.getString(R.string.dialog_edit_category))
                            .setView(v)
                            .setPositiveButton("Ok") { _, _ ->
                                val category = Category(mData.get(p1).getTitle(), mData.get(p1).getMedia())
                                val cv: ContentValues = ContentValues()

                                var image = mData.get(p1).getMedia()

                                cv.put(FIELD_TITLE, t.text.toString())

                                if (imageChanged) {
                                    image = ImageUtils(mActivity).saveThumb(mActivity.pickedImage, true).path.toString()
                                }

                                cv.put(FIELD_IMAGE, image)

                                dbm.update(TBL_NAME_CATEGORIES, cv, category)
                                val nAdapter: CategoryAdapter = CategoryAdapter(mActivity.populateCategoriesDb(), mActivity)
                                mActivity.recycler.adapter = nAdapter
                            }
                            .setNegativeButton(mActivity.resources.getString(R.string.dismiss)) { _, _ ->
                                // dismiss
                            }
                            .create().show()
                }
                R.id.menu_filters -> {
                    mActivity.startActivity<FiltersActivity>("category_id" to dbm.getId(mData[p1]))
                }
            }
            true
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): CategoryHolder {
        val inflatedView = LayoutInflater.from(p0!!.context).inflate(R.layout.category, p0, false)
        return CategoryHolder(inflatedView, mActivity)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onViewDetachedFromWindow(holder: CategoryHolder) {
        holder.clearAnimation()
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mActivity.applicationContext, android.R.anim.fade_in)
            animation.duration = 750
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}