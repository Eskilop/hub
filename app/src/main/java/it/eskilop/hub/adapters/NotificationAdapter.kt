
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.adapters

import android.app.PendingIntent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import it.eskilop.hub.R
import it.eskilop.hub.activities.CategoryActivity
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Notification
import kotlinx.android.synthetic.main.notification_card.view.*


/**
 * Created by eskilop on 07/06/17.
 */
class NotificationAdapter(data: ArrayList<Notification>, activity: CategoryActivity) : RecyclerView.Adapter<NotificationAdapter.NotificationHolder>() {
    var mData: ArrayList<Notification> = data


    class NotificationHolder(v: View, intent: PendingIntent?) : RecyclerView.ViewHolder(v), View.OnClickListener {
        val mTitle: TextView = v.notification_title
        val mMessage: TextView = v.notification_message
        var mIntent: PendingIntent? = intent

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mIntent?.send()
        }
    }

    override fun onBindViewHolder(p0: NotificationHolder, p1: Int) {
        p0.mTitle.text = mData[p1].getTitle()
        p0.mMessage.text = mData[p1].getMessage()
        if (mData[p1].getIntent() != null) {
            p0.mIntent = mData[p1].getIntent()!!
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): NotificationHolder {
        val inflatedView = LayoutInflater.from(p0!!.context).inflate(R.layout.notification_card, p0, false)
        return NotificationHolder(inflatedView, mData[p1].getIntent())
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun getElementAt(index: Int): Notification {
        return mData.get(index)
    }
}