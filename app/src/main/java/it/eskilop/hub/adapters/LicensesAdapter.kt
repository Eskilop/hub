
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.adapters

import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import it.eskilop.hub.R
import it.eskilop.hub.models.License
import kotlinx.android.synthetic.main.license_card.view.*

/**
 * Created by eskilop on 6/27/17.
 */
class LicensesAdapter(data: ArrayList<License>) : RecyclerView.Adapter<LicensesAdapter.LicenseHolder>() {
    private val mData = data

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(p0: LicenseHolder?, p1: Int) {
        p0!!.dev_uname.text = mData[p1].getDevName()
        p0.lib_name.text = mData[p1].getLibName()
        p0.license_text.text = mData[p1].getLicenseText()
        p0.sourcecodebtn.setOnClickListener {
            CustomTabsIntent.Builder()
                    .setToolbarColor(p0.context.resources.getColor(R.color.colorPrimary))
                    .build()
                    .launchUrl(p0.context, Uri.parse(mData[p1].getSourceCodeUrl()))
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): LicenseHolder {
        val inflatedView = LayoutInflater.from(p0!!.context).inflate(R.layout.license_card, p0, false)
        return LicenseHolder(inflatedView)
    }

    class LicenseHolder(v: View) : RecyclerView.ViewHolder(v) {
        val dev_uname = v.developer_uname
        val lib_name = v.lib_name
        val license_text = v.license_text
        val sourcecodebtn = v.source_url
        val context = v.context
    }
}