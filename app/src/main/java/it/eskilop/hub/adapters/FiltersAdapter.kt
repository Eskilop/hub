
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.adapters

import android.content.ContentValues
import android.database.Cursor
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import it.eskilop.hub.R
import it.eskilop.hub.activities.FiltersActivity
import it.eskilop.hub.data.Database
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_CATEGORY
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_EXACT_WORD
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_SENSITIVE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TRIGGER
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_FILTERS
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Category
import it.eskilop.hub.models.Filter
import kotlinx.android.synthetic.main.activity_filters.*
import kotlinx.android.synthetic.main.dialog_filter.view.*
import kotlinx.android.synthetic.main.filter_element.view.*

/**
 * Created by eskilop on 6/16/17.
 */
class FiltersAdapter(data: ArrayList<Filter>, activity: FiltersActivity) : RecyclerView.Adapter<FiltersAdapter.FiltersHolder>() {
    var mData: ArrayList<Filter> = data
    private val mActivity: FiltersActivity = activity
    var dbm: DbManager = DbManager(activity.applicationContext)


    class FiltersHolder(v: View) : RecyclerView.ViewHolder(v) {
        val mFilterTitle: TextView = v.filter_element_name
        val view = v

    }

    override fun onBindViewHolder(p0: FiltersHolder, p1: Int) {
        p0.mFilterTitle.text = mData.get(p1).getTitle()

        p0.view.setOnLongClickListener {
            val v: View = mActivity.layoutInflater.inflate(R.layout.dialog_filter, null)
            var sensitiveChecked = 0
            var exactChecked = 0


            (v.filter_case_sensitive).setOnCheckedChangeListener { _, isChecked ->
                sensitiveChecked = if (isChecked) {
                    1
                } else {
                    0
                }
            }

            (v.filter_exact_word).setOnCheckedChangeListener { _, isChecked ->
                exactChecked = if (isChecked) {
                    1
                } else {
                    0
                }
            }

            (v.filter_name).setText(mData[p1].getTitle())
            (v.filter_trigger).setText(mData[p1].getTrigger())
            (v.filter_case_sensitive).isChecked = mData[p1].getSensitive()
            (v.filter_exact_word).isChecked = mData[p1].getExactWord()
            (v.filter_spinner).adapter = ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, populateSpinner())
            (v.filter_spinner).setSelection(populateSpinner().indexOf(mData[p1].getCategory().getTitle()))

            v.filter_trigger.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    v.filter_name.setText(p0.toString() + " Filter")
                }

            })

            AlertDialog.Builder(v.context)
                    .setTitle(v.context.resources.getString(R.string.dialog_edit_filter))
                    .setView(v)
                    .setPositiveButton("Ok") { _, _ ->
                        val filter = Filter(mActivity, mData[p1].getTrigger(), mData[p1].getCategory(), mData[p1].getTitle(), mData[p1].getIntegerSensitive(), mData[p1].getIntegerExact())
                        val cv = ContentValues()

                        cv.put(FIELD_TITLE, v.filter_name.text.toString())
                        cv.put(FIELD_TRIGGER, v.filter_trigger.text.toString())

                        val selected = (v.filter_spinner).selectedItemPosition

                        cv.put(FIELD_CATEGORY, dbm.getId(populateCategoriesDb()[selected]))
                        cv.put(FIELD_SENSITIVE, sensitiveChecked)
                        cv.put(FIELD_EXACT_WORD, exactChecked)
                        dbm.update(TBL_NAME_FILTERS, cv, filter)
                        mActivity.recycler_filters.adapter = FiltersAdapter(mActivity.getData(), mActivity)
                    }
                    .setNegativeButton(v.context.resources.getString(R.string.dismiss)) { _, _ ->
                        // dismiss
                    }
                    .create().show()
            true
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): FiltersHolder {
        val inflatedView = LayoutInflater.from(p0!!.context).inflate(R.layout.filter_element, p0, false)
        return FiltersHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    private fun populateCategoriesDb(): ArrayList<Category> {
        val dbm = DbManager(mActivity)
        val elements: ArrayList<Category> = ArrayList()
        val crs: Cursor? = dbm.query(Database.DatabaseStrings.TBL_NAME_CATEGORIES)
        while (crs!!.moveToNext()) {
            elements.add(Category(crs.getString(crs.getColumnIndex(FIELD_TITLE)), crs.getString(crs.getColumnIndex(Database.DatabaseStrings.FIELD_IMAGE))))
        }
        return elements
    }

    private fun populateSpinner(): ArrayList<String> {
        val elements: ArrayList<String> = ArrayList()
        val crs: Cursor? = dbm.query(Database.DatabaseStrings.TBL_NAME_CATEGORIES)
        //elements.add("category...")
        while (crs!!.moveToNext()) {
            elements.add(crs.getString(crs.getColumnIndex(FIELD_TITLE)).toString())
        }
        return elements
    }

    fun getElementAt(index: Int): Filter {
        return mData[index]
    }
}
