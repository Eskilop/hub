
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.utils

import android.graphics.Bitmap
import android.graphics.Color

/**
 * Created by eskilop on 16/07/17.
 */


fun lighten(color: Int, fraction: Double): Int {
    val lightenColor = { color: Int, fraction: Double -> Math.min(color + (color * fraction), 255.0).toInt() }
    var red = Color.red(color)
    var green = Color.green(color)
    var blue = Color.blue(color)
    red = lightenColor(red, fraction)
    green = lightenColor(green, fraction)
    blue = lightenColor(blue, fraction)
    val alpha = Color.alpha(color)
    return Color.argb(alpha, red, green, blue)
}

fun darken(color: Int, fraction: Double): Int {
    val darkenColor = { color: Int, fraction: Double -> Math.max(color - (color * fraction), 0.0).toInt() }
    var red = Color.red(color)
    var green = Color.green(color)
    var blue = Color.blue(color)
    red = darkenColor(red, fraction)
    green = darkenColor(green, fraction)
    blue = darkenColor(blue, fraction)
    val alpha = Color.alpha(color)

    return Color.argb(alpha, red, green, blue)
}

fun isDark(color: Int): Boolean {
    val darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255
    return darkness >= 0.5
}

fun isDark(bitmap: Bitmap): Boolean {
    var dark = false

    val darkThreshold = bitmap.width.toFloat() * bitmap.height.toFloat() * 0.45f
    var darkPixels = 0

    val pixels = IntArray(bitmap.width * bitmap.height)
    bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

    for (pixel in pixels) {
        val color = pixel
        val r = Color.red(color)
        val g = Color.green(color)
        val b = Color.blue(color)
        val luminance = 0.299 * r + 0.0 + 0.587 * g + 0.0 + 0.114 * b + 0.0
        if (luminance < 150) {
            darkPixels++
        }
    }

    if (darkPixels >= darkThreshold) {
        dark = true
    }
    return dark
}