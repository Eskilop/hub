
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.graphics.Palette
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


/**
 * Created by eskilop on 14/06/17.
 */
class ImageUtils(context: Context) {
    private val THUMB_DIR: File = context.externalCacheDir

    fun saveThumb(bitmap: Bitmap?, scale: Boolean): File {
        val file = File(THUMB_DIR, generateFileName())


        if (!THUMB_DIR.exists()) {
            THUMB_DIR.mkdir()
        }

        if (!file.exists()) {
            file.createNewFile()
        }
        val out = FileOutputStream(file)

        if (scale) {
            val thumb = Bitmap.createScaledBitmap(bitmap!!, bitmap.width / 2, bitmap.height / 2, false)
            thumb.compress(Bitmap.CompressFormat.JPEG, 80, out)
        } else {
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)
        }

        out.flush()
        out.close()
        return file
    }

    private fun generateFileName(): String {
        val c = Calendar.getInstance()
        return (c.get(Calendar.DAY_OF_WEEK_IN_MONTH) + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND)).toString() + ".jpg"
    }

    // Generate palette synchronously and return it
    fun createPaletteSync(bitmap: Bitmap): Palette {
        val p = Palette.from(bitmap).generate()
        return p
    }
}