
/*
 *    Copyright 2017-2019 Luca D'Amato
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package it.eskilop.hub

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter
import it.eskilop.hub.activities.InfoActivity
import it.eskilop.hub.activities.SettingsActivity
import it.eskilop.hub.adapters.CategoryAdapter
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_IMAGE
import it.eskilop.hub.data.Database.DatabaseStrings.FIELD_TITLE
import it.eskilop.hub.data.Database.DatabaseStrings.TBL_NAME_CATEGORIES
import it.eskilop.hub.data.DbManager
import it.eskilop.hub.models.Category
import it.eskilop.hub.models.Filter
import it.eskilop.hub.utils.ImageUtils
import kotlinx.android.synthetic.main.activity_hub.*
import kotlinx.android.synthetic.main.dialog_category.view.*
import kotlinx.android.synthetic.main.dialog_filter.view.*
import me.toptas.fancyshowcase.DismissListener
import me.toptas.fancyshowcase.FancyShowCaseView
import org.jetbrains.anko.alert
import org.jetbrains.anko.defaultSharedPreferences

class HubActivity : AppCompatActivity() {

    var pickedImage: Bitmap? = null
    private val dbm: DbManager = DbManager(this@HubActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        setContentView(R.layout.activity_hub)
        pickedImage = BitmapFactory.decodeResource(resources, R.drawable.defaultimage)

        if (dbm.isEmpty(TBL_NAME_CATEGORIES)) {
            layoutInsert()
        }

        val notificationListenerChecker = (Settings.Secure.getString(contentResolver, "enabled_notification_listeners") != null && !Settings.Secure.getString(contentResolver, "enabled_notification_listeners").contains(packageName)) || (Settings.Secure.getString(contentResolver, "enabled_notification_listeners") == null)

        if (notificationListenerChecker) {
            alert(R.string.alert_notificationaccess_msg, R.string.alert_notificationaccess_title)
            {
                positiveButton("Ok")
                {
                    startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
                }
            }.show()
        }

        // Initialize FAB
        fab_add.setMenuListener(object : SimpleMenuListenerAdapter() {

            override fun onMenuItemSelected(menuItem: MenuItem?): Boolean {
                if (menuItem != null) {
                    when (menuItem.itemId) {
                        R.id.new_category -> {
                            categoryDialog()
                        }
                        R.id.new_filter -> {
                            filterDialog()
                        }
                    }
                }
                return true
            }

        })

        if (defaultSharedPreferences.getBoolean("isFirstTime", true)) {
            FancyShowCaseView.Builder(this@HubActivity)
                    .focusOn(findViewById(R.id.fab_add))
                    .title(resources.getString(R.string.intro_start))
                    .backgroundColor(Color.argb(200, 0, 0, 0))
                    .dismissListener(object : DismissListener {
                        override fun onSkipped(id: String?) {

                        }

                        override fun onDismiss(id: String?) {
                        }
                    })
                    .build()
                    .show()
        }

        recycler.layoutManager = GridLayoutManager(this@HubActivity, 2)
        recycler.setHasFixedSize(true)
        val nAdapter: CategoryAdapter = CategoryAdapter(populateCategoriesDb(), this@HubActivity)
        recycler.adapter = nAdapter

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val transDown = ObjectAnimator.ofFloat(fab_add, "translationY", 0f, 900f)
                    transDown.duration = 500
                    transDown.start()
                } else {
                    val transUp = ObjectAnimator.ofFloat(fab_add, "translationY", 900f, 0f)
                    transUp.duration = 500
                    transUp.start()
                }
            }

        })

    }

    fun toInteger(exp: Boolean): Int {
        if (!exp) {
            return 0
        } else {
            return 1
        }
    }

    private fun populateSpinner(): ArrayList<String> {
        val dbm: DbManager = DbManager(this@HubActivity)
        val elements: ArrayList<String> = ArrayList()
        val crs: Cursor? = dbm.query(TBL_NAME_CATEGORIES)
        while (crs!!.moveToNext()) {
            elements.add(crs.getString(crs.getColumnIndex(FIELD_TITLE)).toString())
        }
        return elements
    }

    fun populateCategoriesDb(): ArrayList<Category> {
        val dbm = DbManager(this@HubActivity)
        val elements: ArrayList<Category> = ArrayList()
        val crs: Cursor? = dbm.query(TBL_NAME_CATEGORIES)
        while (crs!!.moveToNext()) {
            elements.add(Category(crs.getString(crs.getColumnIndex(FIELD_TITLE)), crs.getString(crs.getColumnIndex(FIELD_IMAGE))))
        }
        return elements
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main, menu)


        if (menu is MenuBuilder) {
            menu.setOptionalIconsVisible(true)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menu_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
            }
            R.id.menu_infos -> {
                startActivity(Intent(this, InfoActivity::class.java))
            }
        }
        return true
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Todo: Display an error
                return
            }
            pickedImage = BitmapFactory.decodeStream(this@HubActivity.contentResolver.openInputStream(data.data))
        }
    }

    fun getImage() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"
        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"
        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(chooserIntent, 11)
    }

    fun layoutDelete() {
        val animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out)
        animation.duration = 750
        empty.visibility = View.INVISIBLE
        empty.startAnimation(animation)
    }

    fun layoutInsert() {
        val animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
        animation.duration = 750
        empty.startAnimation(animation)
        empty.visibility = View.VISIBLE
    }

    fun categoryDialog() {
        val v: View = layoutInflater.inflate(R.layout.dialog_category, null)

        (v.category_imageb).setOnClickListener {
            getImage()
            (v.category_imageb).setImageDrawable(resources.getDrawable(R.drawable.tick))
        }

        val ab = AlertDialog.Builder(this@HubActivity)
                .setTitle(resources.getString(R.string.dialog_category_new_title))
                .setView(v)
                .setPositiveButton("Ok") { _, _ ->
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.category_et.windowToken, 0)
                    if (dbm.isEmpty(TBL_NAME_CATEGORIES)) {
                        layoutDelete()
                    }
                    dbm.save(Category(v.category_et.text.toString(), ImageUtils(this).saveThumb(pickedImage, true).path.toString()))
                    val nAdapter: CategoryAdapter = CategoryAdapter(populateCategoriesDb(), this@HubActivity)
                    recycler.adapter = nAdapter

                    if (defaultSharedPreferences.getBoolean("isFirstTime", true)) {
                        FancyShowCaseView.Builder(this@HubActivity)
                                //.focusOn(findViewById(R.id.fab_add))
                                .title(resources.getString(R.string.intro_new_filter))
                                .backgroundColor(Color.argb(200, 0, 0, 0))
                                .dismissListener(object : DismissListener {
                                    override fun onSkipped(id: String?) {
                                    }

                                    override fun onDismiss(id: String?) {
                                    }
                                })
                                .build()
                                .show()
                    }

                }
                .setNegativeButton(resources.getString(R.string.dismiss)) { _, _ ->
                    // dismiss
                }
        if (!defaultSharedPreferences.getBoolean("isFirstTime", true)) {
            ab.create().show()
        }

        if (defaultSharedPreferences.getBoolean("isFirstTime", true)) {
            FancyShowCaseView.Builder(this@HubActivity)
                    .focusOn(findViewById(R.id.category_et))
                    .title(resources.getString(R.string.intro_new_category_title_image))
                    .backgroundColor(Color.argb(200, 0, 0, 0))
                    .dismissListener(object : DismissListener {
                        override fun onSkipped(id: String?) {
                        }

                        override fun onDismiss(id: String?) {
                            ab.create().show()
                        }
                    })
                    .build()
                    .show()
        }
    }

    fun filterDialog() {
        val v: View = layoutInflater.inflate(R.layout.dialog_filter, null)
        var sensitiveChecked = 0
        var exactChecked = 0

        (v.filter_case_sensitive).setOnCheckedChangeListener { _, isChecked ->
            sensitiveChecked = if (isChecked) {
                1
            } else {
                0
            }
        }

        (v.filter_exact_word).setOnCheckedChangeListener { _, isChecked ->
            exactChecked = if (isChecked) {
                1
            } else {
                0
            }
        }

        if (populateSpinner().size > 0) {

            (v.filter_spinner).adapter = ArrayAdapter<String>(this@HubActivity, android.R.layout.simple_spinner_dropdown_item, populateSpinner())

            v.filter_trigger.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    v.filter_name.setText(p0.toString() + " Filter")
                }

            })

            val ad = AlertDialog.Builder(this@HubActivity)
                    .setTitle(resources.getString(R.string.dialog_filter_new_title))
                    .setView(v)
                    .setPositiveButton("Ok") { _, _ ->

                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.filter_trigger.windowToken, 0)

                        val c = populateCategoriesDb().get((v.filter_spinner).selectedItemPosition)

                        // Save filter
                        if (populateSpinner().isNotEmpty()) {
                            dbm.save(Filter(this@HubActivity, (v.filter_trigger).text.toString(), c, (v.filter_name).text.toString(), sensitiveChecked, exactChecked))
                        }

                        if (defaultSharedPreferences.getBoolean("isFirstTime", true)) {
                            FancyShowCaseView.Builder(this@HubActivity)
                                    .title(resources.getString(R.string.intro_end))
                                    .backgroundColor(Color.argb(200, 0, 0, 0))
                                    .dismissListener(object : DismissListener {
                                        override fun onSkipped(id: String?) {

                                        }

                                        override fun onDismiss(id: String?) {
                                            defaultSharedPreferences.edit().putBoolean("isFirstTime", false).apply()
                                        }
                                    })
                                    .build()
                                    .show()
                        }

                    }
                    .setNegativeButton(resources.getString(R.string.dismiss)) { _, _ ->
                        // dismiss
                    }
            if (!defaultSharedPreferences.getBoolean("isFirstTime", true)) {
                ad.create().show()
            }

            if (defaultSharedPreferences.getBoolean("isFirstTime", true)) {
                FancyShowCaseView.Builder(this@HubActivity)
                        .title(resources.getString(R.string.intro_new_filter_desc))
                        .backgroundColor(Color.argb(200, 0, 0, 0))
                        .dismissListener(object : DismissListener {
                            override fun onSkipped(id: String?) {
                            }

                            override fun onDismiss(id: String?) {
                                ad.create().show()
                            }
                        }).build().show()
            }
        } else {
            alert {
                title(R.string.alert_no_category_title)
                message(R.string.alert_no_category_mesg)
                positiveButton {
                    val v: View = layoutInflater.inflate(R.layout.dialog_category, null)

                    (v.category_imageb).setOnClickListener {
                        getImage()
                        (v.category_imageb).setImageDrawable(resources.getDrawable(R.drawable.tick))
                    }

                    AlertDialog.Builder(this@HubActivity)
                            .setTitle(resources.getString(R.string.dialog_category_new_title))
                            .setView(v)
                            .setPositiveButton("Ok") { _, _ ->
                                if (dbm.isEmpty(TBL_NAME_CATEGORIES)) {
                                    layoutDelete()
                                }
                                dbm.save(Category(v.category_et.text.toString(), ImageUtils(this@HubActivity).saveThumb(pickedImage, true).path.toString()))
                                val nAdapter: CategoryAdapter = CategoryAdapter(populateCategoriesDb(), this@HubActivity)
                                recycler.adapter = nAdapter
                            }
                            .setNegativeButton(resources.getString(R.string.dismiss)) { _, _ ->
                                // dismiss
                            }
                            .create().show()
                }
            }.show()
        }
    }
}
